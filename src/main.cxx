#include "TSystem.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH1D.h"
#include "TROOT.h"
#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <algorithm>
#include "MyNtuple.h"

void copy(MyNtuple* &_out, MyNtuple* &_in){

  _out->LumiWeight=_in->LumiWeight;
  _out->nBJets=_in->nBJets;
  _out->m_1rcjet_kt12=_in->m_1rcjet_kt12;
  _out->m_2rcjet_kt12=_in->m_2rcjet_kt12;
  _out->m_1rcjet_kt8=_in->m_1rcjet_kt8;
  _out->m_2rcjet_kt8=_in->m_2rcjet_kt8;
  _out->dsid=_in->dsid;
  _out->AnalysisWeight=_in->AnalysisWeight;
  _out->pileupweight=_in->pileupweight;
  _out->pileupweightUP=_in->pileupweightUP;
  _out->pileupweightDOWN=_in->pileupweightDOWN;
  _out->pileupweightHash=_in->pileupweightHash;
  _out->nJets=_in->nJets;
  (*_out->jet_pt)=(*_in->jet_pt);
  (*_out->jet_eta)=(*_in->jet_eta);
  (*_out->jet_phi)=(*_in->jet_phi);
  (*_out->jet_e)=(*_in->jet_e);
  (*_out->jet_flav)=(*_in->jet_flav);
  (*_out->jet_truthflav)=(*_in->jet_truthflav);
  (*_out->trackjet_pt)=(*_in->trackjet_pt);
  (*_out->trackjet_eta)=(*_in->trackjet_eta);
  (*_out->trackjet_phi)=(*_in->trackjet_phi);
  (*_out->trackjet_e)=(*_in->trackjet_e);
  (*_out->trackjet_isbjet)=(*_in->trackjet_isbjet);
  (*_out->trackjet_passISROR)=(*_in->trackjet_passISROR);
  (*_out->trackjet_ntrk)=(*_in->trackjet_ntrk);
  (*_out->trackjet_MV2c10)=(*_in->trackjet_MV2c10);
  (*_out->trackjet_flav)=(*_in->trackjet_flav);
  (*_out->trackjet_passDR)=(*_in->trackjet_passDR);
  (*_out->trackjet_truthflav)=(*_in->trackjet_truthflav);
  (*_out->trackjet_truthflavhadexcl)=(*_in->trackjet_truthflavhadexcl);
  (*_out->trackjet_origin)=(*_in->trackjet_origin);
  (*_out->trackjet_type)=(*_in->trackjet_type);
  _out->nbaselineEl=_in->nbaselineEl;
  _out->nEl=_in->nEl;
  (*_out->el_pt)=(*_in->el_pt);
  (*_out->el_eta)=(*_in->el_eta);
  (*_out->el_phi)=(*_in->el_phi);
  (*_out->el_e)=(*_in->el_e);
  (*_out->el_charge)=(*_in->el_charge);
  _out->nbaselineMu=_in->nbaselineMu;
  _out->nMu=_in->nMu;
  (*_out->mu_pt)=(*_in->mu_pt);
  (*_out->mu_eta)=(*_in->mu_eta);
  (*_out->mu_phi)=(*_in->mu_phi);
  (*_out->mu_e)=(*_in->mu_e);
  (*_out->mu_charge)=(*_in->mu_charge);
  _out->nTau=_in->nTau;
  (*_out->tau_pt)=(*_in->tau_pt);
  (*_out->tau_eta)=(*_in->tau_eta);
  (*_out->tau_phi)=(*_in->tau_phi);
  (*_out->tau_e)=(*_in->tau_e);
  (*_out->tau_bdtJet)=(*_in->tau_bdtJet);
  (*_out->tau_bdtEl)=(*_in->tau_bdtEl);
  (*_out->tau_nH)=(*_in->tau_nH);
  (*_out->tau_nTracks)=(*_in->tau_nTracks);
  (*_out->tau_nPi0)=(*_in->tau_nPi0);
  (*_out->tau_nCharged)=(*_in->tau_nCharged);
  (*_out->tau_nNeut)=(*_in->tau_nNeut);
  (*_out->tau_passOR)=(*_in->tau_passOR);
  _out->MET_pt=_in->MET_pt;
  _out->MET_phi=_in->MET_phi;
  _out->MET_pt_prime=_in->MET_pt_prime;
  _out->MET_phi_prime=_in->MET_phi_prime;
  _out->sumet=_in->sumet;
  _out->metsig=_in->metsig;
  _out->metsig_binflate=_in->metsig_binflate;
  _out->metsigST=_in->metsigST;
  _out->metsigET=_in->metsigET;
  _out->metsigHT=_in->metsigHT;
  _out->year=_in->year;
  _out->MuonWeightReco=_in->MuonWeightReco;
  _out->MuonWeightTrigger=_in->MuonWeightTrigger;
  _out->ElecWeightReco=_in->ElecWeightReco;
  _out->ElecWeightTrigger=_in->ElecWeightTrigger;
  _out->TauWeight=_in->TauWeight;
  _out->PrescaleWeight=_in->PrescaleWeight;
  _out->jvtweight=_in->jvtweight;
  _out->btagweight=_in->btagweight;
  _out->btagtrkweight=_in->btagtrkweight;
  _out->averageIntPerXing=_in->averageIntPerXing;
  _out->actualIntPerXing=_in->actualIntPerXing;
  _out->averageIntPerXingCorr=_in->averageIntPerXingCorr;
  (*_out->jet_MV2c10)=(*_in->jet_MV2c10);
  (*_out->jet_passTightClean)=(*_in->jet_passTightClean);
  (*_out->jet_passTightCleanDFFlag)=(*_in->jet_passTightCleanDFFlag);
  (*_out->jet_isSimpleTau)=(*_in->jet_isSimpleTau);
  (*_out->jet_ntracks)=(*_in->jet_ntracks);
  (*_out->jet_truthflavhadexcl)=(*_in->jet_truthflavhadexcl);
  _out->passtauveto=_in->passtauveto;
  _out->MET_jet_pt=_in->MET_jet_pt;
  _out->MET_jet_phi=_in->MET_jet_phi;
  _out->MET_mu_pt=_in->MET_mu_pt;
  _out->MET_mu_phi=_in->MET_mu_phi;
  _out->MET_el_pt=_in->MET_el_pt;
  _out->MET_el_phi=_in->MET_el_phi;
  _out->MET_y_pt=_in->MET_y_pt;
  _out->MET_y_phi=_in->MET_y_phi;
  _out->MET_softTrk_pt=_in->MET_softTrk_pt;
  _out->MET_softTrk_phi=_in->MET_softTrk_phi;
  _out->MET_track_pt=_in->MET_track_pt;
  _out->MET_track_phi=_in->MET_track_phi;
  _out->MET_NonInt_pt=_in->MET_NonInt_pt;
  _out->MET_NonInt_phi=_in->MET_NonInt_phi;
  _out->MET_tau_pt=_in->MET_tau_pt;
  _out->MET_tau_phi=_in->MET_tau_phi;
  _out->sumet_jet=_in->sumet_jet;
  _out->sumet_el=_in->sumet_el;
  _out->sumet_y=_in->sumet_y;
  _out->sumet_mu=_in->sumet_mu;
  _out->sumet_softTrk=_in->sumet_softTrk;
  _out->sumet_softClu=_in->sumet_softClu;
  _out->sumet_NonInt=_in->sumet_NonInt;
  _out->METTrigPassed=_in->METTrigPassed;
  _out->HLT_j400=_in->HLT_j400;
  _out->matched_HLT_j400=_in->matched_HLT_j400;
  _out->HLT_j380=_in->HLT_j380;
  _out->matched_HLT_j380=_in->matched_HLT_j380;
  _out->HLT_xe70_mht=_in->HLT_xe70_mht;
  _out->matched_HLT_xe70_mht=_in->matched_HLT_xe70_mht;
  _out->HLT_xe100_L1XE50=_in->HLT_xe100_L1XE50;
  _out->matched_HLT_xe100_L1XE50=_in->matched_HLT_xe100_L1XE50;
  _out->HLT_xe100_tc_em_L1XE50=_in->HLT_xe100_tc_em_L1XE50;
  _out->matched_HLT_xe100_tc_em_L1XE50=_in->matched_HLT_xe100_tc_em_L1XE50;
  _out->HLT_xe110_mht_L1XE50=_in->HLT_xe110_mht_L1XE50;
  _out->matched_HLT_xe110_mht_L1XE50=_in->matched_HLT_xe110_mht_L1XE50;
  _out->HLT_xe110_pueta_L1XE50=_in->HLT_xe110_pueta_L1XE50;
  _out->matched_HLT_xe110_pueta_L1XE50=_in->matched_HLT_xe110_pueta_L1XE50;
  _out->HLT_xe120_pueta=_in->HLT_xe120_pueta;
  _out->matched_HLT_xe120_pueta=_in->matched_HLT_xe120_pueta;
  _out->HLT_xe120_pufit=_in->HLT_xe120_pufit;
  _out->matched_HLT_xe120_pufit=_in->matched_HLT_xe120_pufit;
  _out->HLT_xe120_tc_lcw_L1XE50=_in->HLT_xe120_tc_lcw_L1XE50;
  _out->matched_HLT_xe120_tc_lcw_L1XE50=_in->matched_HLT_xe120_tc_lcw_L1XE50;
  _out->HLT_xe80_tc_lcw_L1XE50=_in->HLT_xe80_tc_lcw_L1XE50;
  _out->matched_HLT_xe80_tc_lcw_L1XE50=_in->matched_HLT_xe80_tc_lcw_L1XE50;
  _out->HLT_xe90_mht_L1XE50=_in->HLT_xe90_mht_L1XE50;
  _out->matched_HLT_xe90_mht_L1XE50=_in->matched_HLT_xe90_mht_L1XE50;
  _out->HLT_xe90_mht_wEFMu_L1XE50=_in->HLT_xe90_mht_wEFMu_L1XE50;
  _out->matched_HLT_xe90_mht_wEFMu_L1XE50=_in->matched_HLT_xe90_mht_wEFMu_L1XE50;
  _out->HLT_xe90_tc_lcw_wEFMu_L1XE50=_in->HLT_xe90_tc_lcw_wEFMu_L1XE50;
  _out->matched_HLT_xe90_tc_lcw_wEFMu_L1XE50=_in->matched_HLT_xe90_tc_lcw_wEFMu_L1XE50;
  _out->HLT_xe100_pufit_L1XE50=_in->HLT_xe100_pufit_L1XE50;
  _out->matched_HLT_xe100_pufit_L1XE50=_in->matched_HLT_xe100_pufit_L1XE50;
  _out->HLT_xe100_pufit_L1XE55=_in->HLT_xe100_pufit_L1XE55;
  _out->matched_HLT_xe100_pufit_L1XE55=_in->matched_HLT_xe100_pufit_L1XE55;
  _out->HLT_xe110_pufit_L1XE50=_in->HLT_xe110_pufit_L1XE50;
  _out->matched_HLT_xe110_pufit_L1XE50=_in->matched_HLT_xe110_pufit_L1XE50;
  _out->HLT_xe110_pufit_L1XE55=_in->HLT_xe110_pufit_L1XE55;
  _out->matched_HLT_xe110_pufit_L1XE55=_in->matched_HLT_xe110_pufit_L1XE55;
  _out->HLT_xe110_pufit_L1XE60=_in->HLT_xe110_pufit_L1XE60;
  _out->matched_HLT_xe110_pufit_L1XE60=_in->matched_HLT_xe110_pufit_L1XE60;
  _out->HLT_xe120_pufit_L1XE50=_in->HLT_xe120_pufit_L1XE50;
  _out->matched_HLT_xe120_pufit_L1XE50=_in->matched_HLT_xe120_pufit_L1XE50;
  _out->HLT_xe120_pufit_L1XE55=_in->HLT_xe120_pufit_L1XE55;
  _out->matched_HLT_xe120_pufit_L1XE55=_in->matched_HLT_xe120_pufit_L1XE55;
  _out->HLT_xe120_pufit_L1XE60=_in->HLT_xe120_pufit_L1XE60;
  _out->matched_HLT_xe120_pufit_L1XE60=_in->matched_HLT_xe120_pufit_L1XE60;
  _out->HLT_xe90_pufit_L1XE50=_in->HLT_xe90_pufit_L1XE50;
  _out->matched_HLT_xe90_pufit_L1XE50=_in->matched_HLT_xe90_pufit_L1XE50;
  _out->HLT_e24_lhmedium_nod0_L1EM20VH=_in->HLT_e24_lhmedium_nod0_L1EM20VH;
  _out->matched_HLT_e24_lhmedium_nod0_L1EM20VH=_in->matched_HLT_e24_lhmedium_nod0_L1EM20VH;
  _out->HLT_e120_lhloose=_in->HLT_e120_lhloose;
  _out->matched_HLT_e120_lhloose=_in->matched_HLT_e120_lhloose;
  _out->HLT_e140_lhloose_nod0=_in->HLT_e140_lhloose_nod0;
  _out->matched_HLT_e140_lhloose_nod0=_in->matched_HLT_e140_lhloose_nod0;
  _out->HLT_e140_lhloose_nod0_L1EM24VHI=_in->HLT_e140_lhloose_nod0_L1EM24VHI;
  _out->matched_HLT_e140_lhloose_nod0_L1EM24VHI=_in->matched_HLT_e140_lhloose_nod0_L1EM24VHI;
  _out->HLT_e24_lhmedium_L1EM20VH=_in->HLT_e24_lhmedium_L1EM20VH;
  _out->matched_HLT_e24_lhmedium_L1EM20VH=_in->matched_HLT_e24_lhmedium_L1EM20VH;
  _out->HLT_e24_lhtight_nod0_ivarloose=_in->HLT_e24_lhtight_nod0_ivarloose;
  _out->matched_HLT_e24_lhtight_nod0_ivarloose=_in->matched_HLT_e24_lhtight_nod0_ivarloose;
  _out->HLT_e26_lhtight_nod0_ivarloose=_in->HLT_e26_lhtight_nod0_ivarloose;
  _out->matched_HLT_e26_lhtight_nod0_ivarloose=_in->matched_HLT_e26_lhtight_nod0_ivarloose;
  _out->HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM=_in->HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM;
  _out->matched_HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM=_in->matched_HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM;
  _out->HLT_e28_lhtight_nod0_ivarloose=_in->HLT_e28_lhtight_nod0_ivarloose;
  _out->matched_HLT_e28_lhtight_nod0_ivarloose=_in->matched_HLT_e28_lhtight_nod0_ivarloose;
  _out->HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM=_in->HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM;
  _out->matched_HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM=_in->matched_HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM;
  _out->HLT_e300_etcut=_in->HLT_e300_etcut;
  _out->matched_HLT_e300_etcut=_in->matched_HLT_e300_etcut;
  _out->HLT_e300_etcut_L1EM24VHI=_in->HLT_e300_etcut_L1EM24VHI;
  _out->matched_HLT_e300_etcut_L1EM24VHI=_in->matched_HLT_e300_etcut_L1EM24VHI;
  _out->HLT_e60_lhmedium=_in->HLT_e60_lhmedium;
  _out->matched_HLT_e60_lhmedium=_in->matched_HLT_e60_lhmedium;
  _out->HLT_e60_lhmedium_nod0=_in->HLT_e60_lhmedium_nod0;
  _out->matched_HLT_e60_lhmedium_nod0=_in->matched_HLT_e60_lhmedium_nod0;
  _out->HLT_e60_lhmedium_nod0_L1EM24VHI=_in->HLT_e60_lhmedium_nod0_L1EM24VHI;
  _out->matched_HLT_e60_lhmedium_nod0_L1EM24VHI=_in->matched_HLT_e60_lhmedium_nod0_L1EM24VHI;
  _out->HLT_e60_medium=_in->HLT_e60_medium;
  _out->matched_HLT_e60_medium=_in->matched_HLT_e60_medium;
  _out->HLT_mu20_iloose_L1MU15=_in->HLT_mu20_iloose_L1MU15;
  _out->matched_HLT_mu20_iloose_L1MU15=_in->matched_HLT_mu20_iloose_L1MU15;
  _out->HLT_mu24_iloose=_in->HLT_mu24_iloose;
  _out->matched_HLT_mu24_iloose=_in->matched_HLT_mu24_iloose;
  _out->HLT_mu24_iloose_L1MU15=_in->HLT_mu24_iloose_L1MU15;
  _out->matched_HLT_mu24_iloose_L1MU15=_in->matched_HLT_mu24_iloose_L1MU15;
  _out->HLT_mu24_imedium=_in->HLT_mu24_imedium;
  _out->matched_HLT_mu24_imedium=_in->matched_HLT_mu24_imedium;
  _out->HLT_mu24_ivarloose=_in->HLT_mu24_ivarloose;
  _out->matched_HLT_mu24_ivarloose=_in->matched_HLT_mu24_ivarloose;
  _out->HLT_mu24_ivarloose_L1MU15=_in->HLT_mu24_ivarloose_L1MU15;
  _out->matched_HLT_mu24_ivarloose_L1MU15=_in->matched_HLT_mu24_ivarloose_L1MU15;
  _out->HLT_mu24_ivarmedium=_in->HLT_mu24_ivarmedium;
  _out->matched_HLT_mu24_ivarmedium=_in->matched_HLT_mu24_ivarmedium;
  _out->HLT_mu26_imedium=_in->HLT_mu26_imedium;
  _out->matched_HLT_mu26_imedium=_in->matched_HLT_mu26_imedium;
  _out->HLT_mu26_ivarmedium=_in->HLT_mu26_ivarmedium;
  _out->matched_HLT_mu26_ivarmedium=_in->matched_HLT_mu26_ivarmedium;
  _out->HLT_mu40=_in->HLT_mu40;
  _out->matched_HLT_mu40=_in->matched_HLT_mu40;
  _out->HLT_mu50=_in->HLT_mu50;
  _out->matched_HLT_mu50=_in->matched_HLT_mu50;
  _out->HLT_mu60=_in->HLT_mu60;
  _out->matched_HLT_mu60=_in->matched_HLT_mu60;
  _out->HLT_mu60_0eta105_msonly=_in->HLT_mu60_0eta105_msonly;
  _out->matched_HLT_mu60_0eta105_msonly=_in->matched_HLT_mu60_0eta105_msonly;
  _out->HLT_xe100_mht_L1XE50=_in->HLT_xe100_mht_L1XE50;
  _out->matched_HLT_xe100_mht_L1XE50=_in->matched_HLT_xe100_mht_L1XE50;
  _out->HLT_xe70=_in->HLT_xe70;
  _out->matched_HLT_xe70=_in->matched_HLT_xe70;
  _out->HLT_xe70_tc_lcw=_in->HLT_xe70_tc_lcw;
  _out->matched_HLT_xe70_tc_lcw=_in->matched_HLT_xe70_tc_lcw;
  _out->GenFiltMET=_in->GenFiltMET;
  _out->GenFiltHT=_in->GenFiltHT;
  (*_out->rcjet_kt12_pt)=(*_in->rcjet_kt12_pt);
  (*_out->rcjet_kt12_eta)=(*_in->rcjet_kt12_eta);
  (*_out->rcjet_kt12_phi)=(*_in->rcjet_kt12_phi);
  (*_out->rcjet_kt12_e)=(*_in->rcjet_kt12_e);
  (*_out->rcjet_kt12_flav)=(*_in->rcjet_kt12_flav);
  (*_out->rcjet_kt15_pt)=(*_in->rcjet_kt15_pt);
  (*_out->rcjet_kt15_eta)=(*_in->rcjet_kt15_eta);
  (*_out->rcjet_kt15_phi)=(*_in->rcjet_kt15_phi);
  (*_out->rcjet_kt15_e)=(*_in->rcjet_kt15_e);
  (*_out->rcjet_kt15_flav)=(*_in->rcjet_kt15_flav);
  (*_out->rcjet_kt10_pt)=(*_in->rcjet_kt10_pt);
  (*_out->rcjet_kt10_eta)=(*_in->rcjet_kt10_eta);
  (*_out->rcjet_kt10_phi)=(*_in->rcjet_kt10_phi);
  (*_out->rcjet_kt10_e)=(*_in->rcjet_kt10_e);
  (*_out->rcjet_kt10_flav)=(*_in->rcjet_kt10_flav);
  (*_out->rcjet_kt8_pt)=(*_in->rcjet_kt8_pt);
  (*_out->rcjet_kt8_eta)=(*_in->rcjet_kt8_eta);
  (*_out->rcjet_kt8_phi)=(*_in->rcjet_kt8_phi);
  (*_out->rcjet_kt8_e)=(*_in->rcjet_kt8_e);
  (*_out->rcjet_kt8_flav)=(*_in->rcjet_kt8_flav);
  (*_out->fatjet_kt10_pt)=(*_in->fatjet_kt10_pt);
  (*_out->fatjet_kt10_eta)=(*_in->fatjet_kt10_eta);
  (*_out->fatjet_kt10_phi)=(*_in->fatjet_kt10_phi);
  (*_out->fatjet_kt10_e)=(*_in->fatjet_kt10_e);
  (*_out->fatjet_kt10_ntrkjets)=(*_in->fatjet_kt10_ntrkjets);
  (*_out->fatjet_kt10_nghostbhad)=(*_in->fatjet_kt10_nghostbhad);
  (*_out->fatjet_kt10_Split12)=(*_in->fatjet_kt10_Split12);
  (*_out->fatjet_kt10_Split23)=(*_in->fatjet_kt10_Split23);
  (*_out->fatjet_kt10_Split34)=(*_in->fatjet_kt10_Split34);
  (*_out->fatjet_kt10_Qw)=(*_in->fatjet_kt10_Qw);
  (*_out->fatjet_kt10_Tau1)=(*_in->fatjet_kt10_Tau1);
  (*_out->fatjet_kt10_Tau2)=(*_in->fatjet_kt10_Tau2);
  (*_out->fatjet_kt10_Tau3)=(*_in->fatjet_kt10_Tau3);
  (*_out->fatjet_kt10_Tau32)=(*_in->fatjet_kt10_Tau32);
  (*_out->fatjet_kt10_ECF1)=(*_in->fatjet_kt10_ECF1);
  (*_out->fatjet_kt10_ECF2)=(*_in->fatjet_kt10_ECF2);
  (*_out->fatjet_kt10_ECF3)=(*_in->fatjet_kt10_ECF3);
  (*_out->fatjet_kt10_DNNTop80)=(*_in->fatjet_kt10_DNNTop80);
  (*_out->fatjet_kt10_D2)=(*_in->fatjet_kt10_D2);
  (*_out->fatjet_kt10_C2)=(*_in->fatjet_kt10_C2);
  (*_out->fatjet_kt10_origin)=(*_in->fatjet_kt10_origin);
  (*_out->sbv_Lxy)=(*_in->sbv_Lxy);  
  (*_out->sbv_X)=(*_in->sbv_X);
  (*_out->sbv_Y)=(*_in->sbv_Y);
  (*_out->sbv_Z)=(*_in->sbv_Z); 
  (*_out->sbv_nTracks)=(*_in->sbv_nTracks);
  (*_out->sbv_Chi2Reduced)=(*_in->sbv_Chi2Reduced);
  (*_out->sbv_m)=(*_in->sbv_m);
  (*_out->sbv_pt)=(*_in->sbv_pt);
  (*_out->sbv_eta)=(*_in->sbv_eta);
  (*_out->sbv_phi)=(*_in->sbv_phi);
  _out->nsbv=_in->nsbv;
  
}

template <class T, class V> void  resetBranch(T &vector,V value){
  for(unsigned int ii=0; ii<vector->size();ii++){
    vector->at(ii)=value;
  }

}


int main(int argc, char *argv[]) {

  if ( argc < 3) {
    std::cerr << "Usage: <outfolder> <signal> <input1,input2,....>" << std::endl;
    return 1;
  }

  TString m_outfolder = argv[1];
  TString m_signalSample = argv[2];
  std::vector<TString> m_inputs;
  for(int i=3;i<argc;i++){
    m_inputs.push_back(argv[i]);
  }

  std::cout << "m_outfolder=" << m_outfolder <<std::endl;
  std::cout << "m_signalSample=" << m_signalSample  << std::endl;
  for(unsigned int i=0;i<m_inputs.size();i++){
    std::cout << i << " " << m_inputs.at(i) << std::endl;
  }

  TChain* tSignal = new TChain("Nominal");
  tSignal->Add(m_signalSample);

  //for(int i=0; i< 

  MyNtuple* _signal = new MyNtuple(tSignal);
  int nSignal=_signal->fChain->GetEntries();

  std::cout << "nSignal=" << nSignal << std::endl;
 
  TChain* chData = new TChain("Nominal");
  for(unsigned int i=0;i<m_inputs.size();i++){
    chData->Add(m_inputs.at(i));
  }
  int nTotal=chData->GetEntries();
  std::cout << "nTotalData=" << nTotal << std::endl;
  
  float lumi=36200.;
  _signal->GetEntry(0);
  float LumiWeight=_signal->LumiWeight;
  float fraction_of_events=0.8*lumi*LumiWeight;
 
  std::cout << "lumi,lumiweight,frac= " << lumi << " , " << LumiWeight << " , " << fraction_of_events << std::endl;

  int nTotalToSalt=int(nSignal*fraction_of_events);


  int count=0;
  int ntemp=0;
  for(unsigned int i=0;i<m_inputs.size();i++){
 
    TChain* tData = new TChain("Nominal");
    tData->Add(m_inputs.at(i));

    MyNtuple* _data = new MyNtuple(tData);
    int nev =_data->fChain->GetEntries();
    std::cout << i << " " << nev << std::endl;
    TString new_name = m_outfolder + "/" + ((std::string)m_inputs.at(i)).substr(((std::string)m_inputs.at(i)).rfind('/') + 1); 
    TFile* fOut = new TFile(new_name,"RECREATE");
    //fOut->cd();
    MyNtuple* _out = new MyNtuple(((TTree*)tSignal->Clone())->CloneTree(0)); 
    //std::cout << "out " << _out->fChain->GetEntries() << std::endl;

    int nToSalt=int(nTotalToSalt*(nev/float(nTotal)));
   
    std::cout << "nEvents from the signal to put in this file= " << nToSalt << std::endl;
    ntemp+=nToSalt;
    //continue;
    int saltCount=0;
    //nev=10;
    for(int j=0;j<nev;j++){
      if (j%1000 == 0){
	std::cout <<  "on entry = " << j << std::endl;
      }
    
      _data->GetEntry(j);
      copy(_out,_data);
      _out->fChain->Fill();
      // std::cout << nev << std::endl;
      // std::cout << nTotalToSalt << std::endl;
      // std::cout << nToSalt << std::endl;
      // std::cout << nev/nToSalt << std::endl;
      // std::cout << int(nev/nToSalt) << std::endl;
            
      int temp = -1;
      if(nToSalt>0)temp=int(nev/nToSalt);
      
      if (temp>-1 && j%temp == 0 ){
	std::cout << "salting at point j=" << j << std::endl;
	_signal->GetEntry(count);
	copy(_out,_signal);
	
	//hide some variables that'd show up 
	_out->AnalysisWeight=1.0;
	_out->LumiWeight=0;
	_out->year= _data->year;
	//int tempDSID= _out->dsid;
	_out->dsid= _data->dsid;
	
	_out->GenFiltMET=_data->GenFiltMET;
	_out->GenFiltHT=-2.4092019e-185;
	_out->MET_NonInt_pt=0;
	_out->MET_NonInt_phi=_data->MET_NonInt_phi;

	
	resetBranch(_out->jet_truthflav,-1);
	resetBranch(_out->trackjet_truthflav,-1);
	resetBranch(_out->trackjet_truthflavhadexcl,-999);
	
	_out->fChain->Fill();
       	saltCount+=1;
       	count+=1;
	
      }
     
    }
    //_out->fChain->SetAlias("test","dsid");
    
    std::cout << "at the end of the loop now " << new_name <<  std::endl;
 
    fOut->Write();
    fOut->Close();
  }
  std::cout << nTotalToSalt << " " << ntemp << std::endl;
  return 0;
}
