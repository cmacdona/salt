CC := g++ # This is the main compiler
SRCDIR := src
BUILDDIR := build
TARGET := bin/runFakeData
SRCEXT := cxx
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

CFLAGS := -Wall -O3  # -g
LIB := $(shell root-config --libs)
#INC := -I include -I$(ROOTSYS)/include -I /cvmfs/sft.cern.ch/lcg/views/LCG_rootext20180517/x86_64-slc6-gcc62-opt/include
INC := -I include -I$(ROOTSYS)/include -I$(CMAKE_PREFIX_PATH)/include 


$(TARGET): $(OBJECTS) 
	@echo " Linking..."
	@echo " $(CC) $^ -o $(TARGET) $(LIB)"; $(CC) $^ -o $(TARGET) $(LIB)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@  $<

clean:
	@echo " Cleaning..."; 
	@echo " $(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)

.PHONY: clean
