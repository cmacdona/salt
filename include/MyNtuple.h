//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jul  1 10:40:54 2019 by ROOT version 6.19/01
// from TTree Nominal/Nominal
// found on file: /atlas/shatlas/NTUP_SUSY/AnaChallenge/21.2.75-24ac293/Step1_AddXSec/mc16_13TeV.389525.MGPy8EG_A14N23LO_TT_pMSSM_M22M1_nMU_850_250.e5576_a875_r9364_p3875_anaChallenge.root
//////////////////////////////////////////////////////////

#ifndef MyNtuple_h
#define MyNtuple_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"

using namespace std;

class MyNtuple {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Double_t        LumiWeight;
   Int_t           nBJets;
   Double_t        m_1rcjet_kt12;
   Double_t        m_2rcjet_kt12;
   Double_t        m_1rcjet_kt8;
   Double_t        m_2rcjet_kt8;
   Int_t           dsid;
   Float_t         AnalysisWeight;
   Float_t         pileupweight;
   Float_t         pileupweightUP;
   Float_t         pileupweightDOWN;
   ULong64_t       pileupweightHash;
   Int_t           nJets;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<int>     *jet_flav;
   vector<int>     *jet_truthflav;
   vector<float>   *trackjet_pt;
   vector<float>   *trackjet_eta;
   vector<float>   *trackjet_phi;
   vector<float>   *trackjet_e;
   vector<int>     *trackjet_isbjet;
   vector<int>     *trackjet_passISROR;
   vector<int>     *trackjet_ntrk;
   vector<float>   *trackjet_MV2c10;
   vector<int>     *trackjet_flav;
   vector<int>     *trackjet_passDR;
   vector<int>     *trackjet_truthflav;
   vector<int>     *trackjet_truthflavhadexcl;
   vector<string>  *trackjet_origin;
   vector<string>  *trackjet_type;
   Int_t           nbaselineEl;
   Int_t           nEl;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   Int_t           nbaselineMu;
   Int_t           nMu;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   Int_t           nTau;
   vector<float>   *tau_pt;
   vector<float>   *tau_eta;
   vector<float>   *tau_phi;
   vector<float>   *tau_e;
   vector<float>   *tau_bdtJet;
   vector<float>   *tau_bdtEl;
   vector<float>   *tau_nH;
   vector<float>   *tau_nTracks;
   vector<float>   *tau_nPi0;
   vector<float>   *tau_nCharged;
   vector<float>   *tau_nNeut;
   vector<int>     *tau_passOR;
   Float_t         MET_pt;
   Float_t         MET_phi;
   Float_t         MET_pt_prime;
   Float_t         MET_phi_prime;
   Float_t         sumet;
   Float_t         metsig;
   Float_t         metsig_binflate;
   Float_t         metsigST;
   Float_t         metsigET;
   Float_t         metsigHT;
   Int_t           year;
   Float_t         MuonWeightReco;
   Float_t         MuonWeightTrigger;
   Float_t         ElecWeightReco;
   Float_t         ElecWeightTrigger;
   Float_t         TauWeight;
   Float_t         PrescaleWeight;
   Float_t         jvtweight;
   Float_t         btagweight;
   Float_t         btagtrkweight;
   Float_t         averageIntPerXing;
   Float_t         actualIntPerXing;
   Float_t         averageIntPerXingCorr;
   vector<float>   *jet_MV2c10;
   vector<int>     *jet_passTightClean;
   vector<int>     *jet_passTightCleanDFFlag;
   vector<int>     *jet_isSimpleTau;
   vector<int>     *jet_ntracks;
   vector<int>     *jet_truthflavhadexcl;
   Int_t           passtauveto;
   Float_t         MET_jet_pt;
   Float_t         MET_jet_phi;
   Float_t         MET_mu_pt;
   Float_t         MET_mu_phi;
   Float_t         MET_el_pt;
   Float_t         MET_el_phi;
   Float_t         MET_y_pt;
   Float_t         MET_y_phi;
   Float_t         MET_softTrk_pt;
   Float_t         MET_softTrk_phi;
   Float_t         MET_track_pt;
   Float_t         MET_track_phi;
   Float_t         MET_NonInt_pt;
   Float_t         MET_NonInt_phi;
   Float_t         MET_tau_pt;
   Float_t         MET_tau_phi;
   Float_t         sumet_jet;
   Float_t         sumet_el;
   Float_t         sumet_y;
   Float_t         sumet_mu;
   Float_t         sumet_softTrk;
   Float_t         sumet_softClu;
   Float_t         sumet_NonInt;
   Float_t         METTrigPassed;
   Bool_t          HLT_j400;
   Bool_t          matched_HLT_j400;
   Bool_t          HLT_j380;
   Bool_t          matched_HLT_j380;
   Bool_t          HLT_xe70_mht;
   Bool_t          matched_HLT_xe70_mht;
   Bool_t          HLT_xe100_L1XE50;
   Bool_t          matched_HLT_xe100_L1XE50;
   Bool_t          HLT_xe100_tc_em_L1XE50;
   Bool_t          matched_HLT_xe100_tc_em_L1XE50;
   Bool_t          HLT_xe110_mht_L1XE50;
   Bool_t          matched_HLT_xe110_mht_L1XE50;
   Bool_t          HLT_xe110_pueta_L1XE50;
   Bool_t          matched_HLT_xe110_pueta_L1XE50;
   Bool_t          HLT_xe120_pueta;
   Bool_t          matched_HLT_xe120_pueta;
   Bool_t          HLT_xe120_pufit;
   Bool_t          matched_HLT_xe120_pufit;
   Bool_t          HLT_xe120_tc_lcw_L1XE50;
   Bool_t          matched_HLT_xe120_tc_lcw_L1XE50;
   Bool_t          HLT_xe80_tc_lcw_L1XE50;
   Bool_t          matched_HLT_xe80_tc_lcw_L1XE50;
   Bool_t          HLT_xe90_mht_L1XE50;
   Bool_t          matched_HLT_xe90_mht_L1XE50;
   Bool_t          HLT_xe90_mht_wEFMu_L1XE50;
   Bool_t          matched_HLT_xe90_mht_wEFMu_L1XE50;
   Bool_t          HLT_xe90_tc_lcw_wEFMu_L1XE50;
   Bool_t          matched_HLT_xe90_tc_lcw_wEFMu_L1XE50;
   Bool_t          HLT_xe100_pufit_L1XE50;
   Bool_t          matched_HLT_xe100_pufit_L1XE50;
   Bool_t          HLT_xe100_pufit_L1XE55;
   Bool_t          matched_HLT_xe100_pufit_L1XE55;
   Bool_t          HLT_xe110_pufit_L1XE50;
   Bool_t          matched_HLT_xe110_pufit_L1XE50;
   Bool_t          HLT_xe110_pufit_L1XE55;
   Bool_t          matched_HLT_xe110_pufit_L1XE55;
   Bool_t          HLT_xe110_pufit_L1XE60;
   Bool_t          matched_HLT_xe110_pufit_L1XE60;
   Bool_t          HLT_xe120_pufit_L1XE50;
   Bool_t          matched_HLT_xe120_pufit_L1XE50;
   Bool_t          HLT_xe120_pufit_L1XE55;
   Bool_t          matched_HLT_xe120_pufit_L1XE55;
   Bool_t          HLT_xe120_pufit_L1XE60;
   Bool_t          matched_HLT_xe120_pufit_L1XE60;
   Bool_t          HLT_xe90_pufit_L1XE50;
   Bool_t          matched_HLT_xe90_pufit_L1XE50;
   Bool_t          HLT_e24_lhmedium_nod0_L1EM20VH;
   Bool_t          matched_HLT_e24_lhmedium_nod0_L1EM20VH;
   Bool_t          HLT_e120_lhloose;
   Bool_t          matched_HLT_e120_lhloose;
   Bool_t          HLT_e140_lhloose_nod0;
   Bool_t          matched_HLT_e140_lhloose_nod0;
   Bool_t          HLT_e140_lhloose_nod0_L1EM24VHI;
   Bool_t          matched_HLT_e140_lhloose_nod0_L1EM24VHI;
   Bool_t          HLT_e24_lhmedium_L1EM20VH;
   Bool_t          matched_HLT_e24_lhmedium_L1EM20VH;
   Bool_t          HLT_e24_lhtight_nod0_ivarloose;
   Bool_t          matched_HLT_e24_lhtight_nod0_ivarloose;
   Bool_t          HLT_e26_lhtight_nod0_ivarloose;
   Bool_t          matched_HLT_e26_lhtight_nod0_ivarloose;
   Bool_t          HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM;
   Bool_t          matched_HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM;
   Bool_t          HLT_e28_lhtight_nod0_ivarloose;
   Bool_t          matched_HLT_e28_lhtight_nod0_ivarloose;
   Bool_t          HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM;
   Bool_t          matched_HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM;
   Bool_t          HLT_e300_etcut;
   Bool_t          matched_HLT_e300_etcut;
   Bool_t          HLT_e300_etcut_L1EM24VHI;
   Bool_t          matched_HLT_e300_etcut_L1EM24VHI;
   Bool_t          HLT_e60_lhmedium;
   Bool_t          matched_HLT_e60_lhmedium;
   Bool_t          HLT_e60_lhmedium_nod0;
   Bool_t          matched_HLT_e60_lhmedium_nod0;
   Bool_t          HLT_e60_lhmedium_nod0_L1EM24VHI;
   Bool_t          matched_HLT_e60_lhmedium_nod0_L1EM24VHI;
   Bool_t          HLT_e60_medium;
   Bool_t          matched_HLT_e60_medium;
   Bool_t          HLT_mu20_iloose_L1MU15;
   Bool_t          matched_HLT_mu20_iloose_L1MU15;
   Bool_t          HLT_mu24_iloose;
   Bool_t          matched_HLT_mu24_iloose;
   Bool_t          HLT_mu24_iloose_L1MU15;
   Bool_t          matched_HLT_mu24_iloose_L1MU15;
   Bool_t          HLT_mu24_imedium;
   Bool_t          matched_HLT_mu24_imedium;
   Bool_t          HLT_mu24_ivarloose;
   Bool_t          matched_HLT_mu24_ivarloose;
   Bool_t          HLT_mu24_ivarloose_L1MU15;
   Bool_t          matched_HLT_mu24_ivarloose_L1MU15;
   Bool_t          HLT_mu24_ivarmedium;
   Bool_t          matched_HLT_mu24_ivarmedium;
   Bool_t          HLT_mu26_imedium;
   Bool_t          matched_HLT_mu26_imedium;
   Bool_t          HLT_mu26_ivarmedium;
   Bool_t          matched_HLT_mu26_ivarmedium;
   Bool_t          HLT_mu40;
   Bool_t          matched_HLT_mu40;
   Bool_t          HLT_mu50;
   Bool_t          matched_HLT_mu50;
   Bool_t          HLT_mu60;
   Bool_t          matched_HLT_mu60;
   Bool_t          HLT_mu60_0eta105_msonly;
   Bool_t          matched_HLT_mu60_0eta105_msonly;
   Bool_t          HLT_xe100_mht_L1XE50;
   Bool_t          matched_HLT_xe100_mht_L1XE50;
   Bool_t          HLT_xe70;
   Bool_t          matched_HLT_xe70;
   Bool_t          HLT_xe70_tc_lcw;
   Bool_t          matched_HLT_xe70_tc_lcw;
   Double_t        GenFiltMET;
   Double_t        GenFiltHT;
   vector<float>   *rcjet_kt12_pt;
   vector<float>   *rcjet_kt12_eta;
   vector<float>   *rcjet_kt12_phi;
   vector<float>   *rcjet_kt12_e;
   vector<int>     *rcjet_kt12_flav;
   vector<float>   *rcjet_kt15_pt;
   vector<float>   *rcjet_kt15_eta;
   vector<float>   *rcjet_kt15_phi;
   vector<float>   *rcjet_kt15_e;
   vector<int>     *rcjet_kt15_flav;
   vector<float>   *rcjet_kt10_pt;
   vector<float>   *rcjet_kt10_eta;
   vector<float>   *rcjet_kt10_phi;
   vector<float>   *rcjet_kt10_e;
   vector<int>     *rcjet_kt10_flav;
   vector<float>   *rcjet_kt8_pt;
   vector<float>   *rcjet_kt8_eta;
   vector<float>   *rcjet_kt8_phi;
   vector<float>   *rcjet_kt8_e;
   vector<int>     *rcjet_kt8_flav;
   vector<float>   *fatjet_kt10_pt;
   vector<float>   *fatjet_kt10_eta;
   vector<float>   *fatjet_kt10_phi;
   vector<float>   *fatjet_kt10_e;
   vector<int>     *fatjet_kt10_ntrkjets;
   vector<int>     *fatjet_kt10_nghostbhad;
   vector<float>   *fatjet_kt10_Split12;
   vector<float>   *fatjet_kt10_Split23;
   vector<float>   *fatjet_kt10_Split34;
   vector<float>   *fatjet_kt10_Qw;
   vector<float>   *fatjet_kt10_Tau1;
   vector<float>   *fatjet_kt10_Tau2;
   vector<float>   *fatjet_kt10_Tau3;
   vector<float>   *fatjet_kt10_Tau32;
   vector<float>   *fatjet_kt10_ECF1;
   vector<float>   *fatjet_kt10_ECF2;
   vector<float>   *fatjet_kt10_ECF3;
   vector<int>     *fatjet_kt10_DNNTop80;
   vector<float>   *fatjet_kt10_D2;
   vector<float>   *fatjet_kt10_C2;
   vector<string>  *fatjet_kt10_origin;
   vector<float>   *sbv_Lxy;
   vector<float>   *sbv_X;
   vector<float>   *sbv_Y;
   vector<float>   *sbv_Z;
   vector<int>     *sbv_nTracks;
   vector<float>   *sbv_Chi2Reduced;
   vector<float>   *sbv_m;
   vector<float>   *sbv_pt;
   vector<float>   *sbv_eta;
   vector<float>   *sbv_phi;
   Int_t           nsbv;

   // List of branches
   TBranch        *b_LumiWeight;   //!
   TBranch        *b_nBJets;   //!
   TBranch        *b_m_1rcjet_kt12;   //!
   TBranch        *b_m_2rcjet_kt12;   //!
   TBranch        *b_m_1rcjet_kt8;   //!
   TBranch        *b_m_2rcjet_kt8;   //!
   TBranch        *b_dsid;   //!
   TBranch        *b_AnalysisWeight;   //!
   TBranch        *b_pileupweight;   //!
   TBranch        *b_pileupweightUP;   //!
   TBranch        *b_pileupweightDOWN;   //!
   TBranch        *b_pileupweightHash;   //!
   TBranch        *b_nJets;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_flav;   //!
   TBranch        *b_jet_truthflav;   //!
   TBranch        *b_trackjet_pt;   //!
   TBranch        *b_trackjet_eta;   //!
   TBranch        *b_trackjet_phi;   //!
   TBranch        *b_trackjet_e;   //!
   TBranch        *b_trackjet_isbjet;   //!
   TBranch        *b_trackjet_passISROR;   //!
   TBranch        *b_trackjet_ntrk;   //!
   TBranch        *b_trackjet_MV2c10;   //!
   TBranch        *b_trackjet_flav;   //!
   TBranch        *b_trackjet_passDR;   //!
   TBranch        *b_trackjet_truthflav;   //!
   TBranch        *b_trackjet_truthflavhadexcl;   //!
   TBranch        *b_trackjet_origin;   //!
   TBranch        *b_trackjet_type;   //!
   TBranch        *b_nbaselineEl;   //!
   TBranch        *b_nEl;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_nbaselineMu;   //!
   TBranch        *b_nMu;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_nTau;   //!
   TBranch        *b_tau_pt;   //!
   TBranch        *b_tau_eta;   //!
   TBranch        *b_tau_phi;   //!
   TBranch        *b_tau_e;   //!
   TBranch        *b_tau_bdtJet;   //!
   TBranch        *b_tau_bdtEl;   //!
   TBranch        *b_tau_nH;   //!
   TBranch        *b_tau_nTracks;   //!
   TBranch        *b_tau_nPi0;   //!
   TBranch        *b_tau_nCharged;   //!
   TBranch        *b_tau_nNeut;   //!
   TBranch        *b_tau_passOR;   //!
   TBranch        *b_MET_pt;   //!
   TBranch        *b_MET_phi;   //!
   TBranch        *b_MET_pt_prime;   //!
   TBranch        *b_MET_phi_prime;   //!
   TBranch        *b_sumet;   //!
   TBranch        *b_metsig;   //!
   TBranch        *b_metsig_binflate;   //!
   TBranch        *b_metsigST;   //!
   TBranch        *b_metsigET;   //!
   TBranch        *b_metsigHT;   //!
   TBranch        *b_year;   //!
   TBranch        *b_MuonWeightReco;   //!
   TBranch        *b_MuonWeightTrigger;   //!
   TBranch        *b_ElecWeightReco;   //!
   TBranch        *b_ElecWeightTrigger;   //!
   TBranch        *b_TauWeight;   //!
   TBranch        *b_PrescaleWeight;   //!
   TBranch        *b_jvtweight;   //!
   TBranch        *b_btagweight;   //!
   TBranch        *b_btagtrkweight;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_averageIntPerXingCorr;   //!
   TBranch        *b_jet_MV2c10;   //!
   TBranch        *b_jet_passTightClean;   //!
   TBranch        *b_jet_passTightCleanDFFlag;   //!
   TBranch        *b_jet_isSimpleTau;   //!
   TBranch        *b_jet_ntracks;   //!
   TBranch        *b_jet_truthflavhadexcl;   //!
   TBranch        *b_passtauveto;   //!
   TBranch        *b_MET_jet_pt;   //!
   TBranch        *b_MET_jet_phi;   //!
   TBranch        *b_MET_mu_pt;   //!
   TBranch        *b_MET_mu_phi;   //!
   TBranch        *b_MET_el_pt;   //!
   TBranch        *b_MET_el_phi;   //!
   TBranch        *b_MET_y_pt;   //!
   TBranch        *b_MET_y_phi;   //!
   TBranch        *b_MET_softTrk_pt;   //!
   TBranch        *b_MET_softTrk_phi;   //!
   TBranch        *b_MET_track_pt;   //!
   TBranch        *b_MET_track_phi;   //!
   TBranch        *b_MET_NonInt_pt;   //!
   TBranch        *b_MET_NonInt_phi;   //!
   TBranch        *b_MET_tau_pt;   //!
   TBranch        *b_MET_tau_phi;   //!
   TBranch        *b_sumet_jet;   //!
   TBranch        *b_sumet_el;   //!
   TBranch        *b_sumet_y;   //!
   TBranch        *b_sumet_mu;   //!
   TBranch        *b_sumet_softTrk;   //!
   TBranch        *b_sumet_softClu;   //!
   TBranch        *b_sumet_NonInt;   //!
   TBranch        *b_METTrigPassed;   //!
   TBranch        *b_HLT_j400;   //!
   TBranch        *b_matched_HLT_j400;   //!
   TBranch        *b_HLT_j380;   //!
   TBranch        *b_matched_HLT_j380;   //!
   TBranch        *b_HLT_xe70_mht;   //!
   TBranch        *b_matched_HLT_xe70_mht;   //!
   TBranch        *b_HLT_xe100_L1XE50;   //!
   TBranch        *b_matched_HLT_xe100_L1XE50;   //!
   TBranch        *b_HLT_xe100_tc_em_L1XE50;   //!
   TBranch        *b_matched_HLT_xe100_tc_em_L1XE50;   //!
   TBranch        *b_HLT_xe110_mht_L1XE50;   //!
   TBranch        *b_matched_HLT_xe110_mht_L1XE50;   //!
   TBranch        *b_HLT_xe110_pueta_L1XE50;   //!
   TBranch        *b_matched_HLT_xe110_pueta_L1XE50;   //!
   TBranch        *b_HLT_xe120_pueta;   //!
   TBranch        *b_matched_HLT_xe120_pueta;   //!
   TBranch        *b_HLT_xe120_pufit;   //!
   TBranch        *b_matched_HLT_xe120_pufit;   //!
   TBranch        *b_HLT_xe120_tc_lcw_L1XE50;   //!
   TBranch        *b_matched_HLT_xe120_tc_lcw_L1XE50;   //!
   TBranch        *b_HLT_xe80_tc_lcw_L1XE50;   //!
   TBranch        *b_matched_HLT_xe80_tc_lcw_L1XE50;   //!
   TBranch        *b_HLT_xe90_mht_L1XE50;   //!
   TBranch        *b_matched_HLT_xe90_mht_L1XE50;   //!
   TBranch        *b_HLT_xe90_mht_wEFMu_L1XE50;   //!
   TBranch        *b_matched_HLT_xe90_mht_wEFMu_L1XE50;   //!
   TBranch        *b_HLT_xe90_tc_lcw_wEFMu_L1XE50;   //!
   TBranch        *b_matched_HLT_xe90_tc_lcw_wEFMu_L1XE50;   //!
   TBranch        *b_HLT_xe100_pufit_L1XE50;   //!
   TBranch        *b_matched_HLT_xe100_pufit_L1XE50;   //!
   TBranch        *b_HLT_xe100_pufit_L1XE55;   //!
   TBranch        *b_matched_HLT_xe100_pufit_L1XE55;   //!
   TBranch        *b_HLT_xe110_pufit_L1XE50;   //!
   TBranch        *b_matched_HLT_xe110_pufit_L1XE50;   //!
   TBranch        *b_HLT_xe110_pufit_L1XE55;   //!
   TBranch        *b_matched_HLT_xe110_pufit_L1XE55;   //!
   TBranch        *b_HLT_xe110_pufit_L1XE60;   //!
   TBranch        *b_matched_HLT_xe110_pufit_L1XE60;   //!
   TBranch        *b_HLT_xe120_pufit_L1XE50;   //!
   TBranch        *b_matched_HLT_xe120_pufit_L1XE50;   //!
   TBranch        *b_HLT_xe120_pufit_L1XE55;   //!
   TBranch        *b_matched_HLT_xe120_pufit_L1XE55;   //!
   TBranch        *b_HLT_xe120_pufit_L1XE60;   //!
   TBranch        *b_matched_HLT_xe120_pufit_L1XE60;   //!
   TBranch        *b_HLT_xe90_pufit_L1XE50;   //!
   TBranch        *b_matched_HLT_xe90_pufit_L1XE50;   //!
   TBranch        *b_HLT_e24_lhmedium_nod0_L1EM20VH;   //!
   TBranch        *b_matched_HLT_e24_lhmedium_nod0_L1EM20VH;   //!
   TBranch        *b_HLT_e120_lhloose;   //!
   TBranch        *b_matched_HLT_e120_lhloose;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_matched_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_e140_lhloose_nod0_L1EM24VHI;   //!
   TBranch        *b_matched_HLT_e140_lhloose_nod0_L1EM24VHI;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_matched_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_HLT_e24_lhtight_nod0_ivarloose;   //!
   TBranch        *b_matched_HLT_e24_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_matched_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM;   //!
   TBranch        *b_matched_HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM;   //!
   TBranch        *b_HLT_e28_lhtight_nod0_ivarloose;   //!
   TBranch        *b_matched_HLT_e28_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM;   //!
   TBranch        *b_matched_HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM;   //!
   TBranch        *b_HLT_e300_etcut;   //!
   TBranch        *b_matched_HLT_e300_etcut;   //!
   TBranch        *b_HLT_e300_etcut_L1EM24VHI;   //!
   TBranch        *b_matched_HLT_e300_etcut_L1EM24VHI;   //!
   TBranch        *b_HLT_e60_lhmedium;   //!
   TBranch        *b_matched_HLT_e60_lhmedium;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_matched_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0_L1EM24VHI;   //!
   TBranch        *b_matched_HLT_e60_lhmedium_nod0_L1EM24VHI;   //!
   TBranch        *b_HLT_e60_medium;   //!
   TBranch        *b_matched_HLT_e60_medium;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_matched_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_mu24_iloose;   //!
   TBranch        *b_matched_HLT_mu24_iloose;   //!
   TBranch        *b_HLT_mu24_iloose_L1MU15;   //!
   TBranch        *b_matched_HLT_mu24_iloose_L1MU15;   //!
   TBranch        *b_HLT_mu24_imedium;   //!
   TBranch        *b_matched_HLT_mu24_imedium;   //!
   TBranch        *b_HLT_mu24_ivarloose;   //!
   TBranch        *b_matched_HLT_mu24_ivarloose;   //!
   TBranch        *b_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_matched_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_HLT_mu24_ivarmedium;   //!
   TBranch        *b_matched_HLT_mu24_ivarmedium;   //!
   TBranch        *b_HLT_mu26_imedium;   //!
   TBranch        *b_matched_HLT_mu26_imedium;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_matched_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_mu40;   //!
   TBranch        *b_matched_HLT_mu40;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_matched_HLT_mu50;   //!
   TBranch        *b_HLT_mu60;   //!
   TBranch        *b_matched_HLT_mu60;   //!
   TBranch        *b_HLT_mu60_0eta105_msonly;   //!
   TBranch        *b_matched_HLT_mu60_0eta105_msonly;   //!
   TBranch        *b_HLT_xe100_mht_L1XE50;   //!
   TBranch        *b_matched_HLT_xe100_mht_L1XE50;   //!
   TBranch        *b_HLT_xe70;   //!
   TBranch        *b_matched_HLT_xe70;   //!
   TBranch        *b_HLT_xe70_tc_lcw;   //!
   TBranch        *b_matched_HLT_xe70_tc_lcw;   //!
   TBranch        *b_GenFiltMET;   //!
   TBranch        *b_GenFiltHT;   //!
   TBranch        *b_rcjet_kt12_pt;   //!
   TBranch        *b_rcjet_kt12_eta;   //!
   TBranch        *b_rcjet_kt12_phi;   //!
   TBranch        *b_rcjet_kt12_e;   //!
   TBranch        *b_rcjet_kt12_flav;   //!
   TBranch        *b_rcjet_kt15_pt;   //!
   TBranch        *b_rcjet_kt15_eta;   //!
   TBranch        *b_rcjet_kt15_phi;   //!
   TBranch        *b_rcjet_kt15_e;   //!
   TBranch        *b_rcjet_kt15_flav;   //!
   TBranch        *b_rcjet_kt10_pt;   //!
   TBranch        *b_rcjet_kt10_eta;   //!
   TBranch        *b_rcjet_kt10_phi;   //!
   TBranch        *b_rcjet_kt10_e;   //!
   TBranch        *b_rcjet_kt10_flav;   //!
   TBranch        *b_rcjet_kt8_pt;   //!
   TBranch        *b_rcjet_kt8_eta;   //!
   TBranch        *b_rcjet_kt8_phi;   //!
   TBranch        *b_rcjet_kt8_e;   //!
   TBranch        *b_rcjet_kt8_flav;   //!
   TBranch        *b_fatjet_kt10_pt;   //!
   TBranch        *b_fatjet_kt10_eta;   //!
   TBranch        *b_fatjet_kt10_phi;   //!
   TBranch        *b_fatjet_kt10_e;   //!
   TBranch        *b_fatjet_kt10_ntrkjets;   //!
   TBranch        *b_fatjet_kt10_nghostbhad;   //!
   TBranch        *b_fatjet_kt10_Split12;   //!
   TBranch        *b_fatjet_kt10_Split23;   //!
   TBranch        *b_fatjet_kt10_Split34;   //!
   TBranch        *b_fatjet_kt10_Qw;   //!
   TBranch        *b_fatjet_kt10_Tau1;   //!
   TBranch        *b_fatjet_kt10_Tau2;   //!
   TBranch        *b_fatjet_kt10_Tau3;   //!
   TBranch        *b_fatjet_kt10_Tau32;   //!
   TBranch        *b_fatjet_kt10_ECF1;   //!
   TBranch        *b_fatjet_kt10_ECF2;   //!
   TBranch        *b_fatjet_kt10_ECF3;   //!
   TBranch        *b_fatjet_kt10_DNNTop80;   //!
   TBranch        *b_fatjet_kt10_D2;   //!
   TBranch        *b_fatjet_kt10_C2;   //!
   TBranch        *b_fatjet_kt10_origin;   //!
   TBranch        *b_sbv_Lxy;   //!
   TBranch        *b_sbv_X;   //!
   TBranch        *b_sbv_Y;   //!
   TBranch        *b_sbv_Z;   //!
   TBranch        *b_sbv_nTracks;   //!
   TBranch        *b_sbv_Chi2Reduced;   //!
   TBranch        *b_sbv_m;   //!
   TBranch        *b_sbv_pt;   //!
   TBranch        *b_sbv_eta;   //!
   TBranch        *b_sbv_phi;   //!
   TBranch        *b_nsbv;   //!

   MyNtuple(TTree *tree=0);
   virtual ~MyNtuple();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef MyNtuple_cxx
MyNtuple::MyNtuple(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/atlas/shatlas/NTUP_SUSY/AnaChallenge/21.2.75-24ac293/Step1_AddXSec/mc16_13TeV.389525.MGPy8EG_A14N23LO_TT_pMSSM_M22M1_nMU_850_250.e5576_a875_r9364_p3875_anaChallenge.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/atlas/shatlas/NTUP_SUSY/AnaChallenge/21.2.75-24ac293/Step1_AddXSec/mc16_13TeV.389525.MGPy8EG_A14N23LO_TT_pMSSM_M22M1_nMU_850_250.e5576_a875_r9364_p3875_anaChallenge.root");
      }
      f->GetObject("Nominal",tree);

   }
   Init(tree);
}

MyNtuple::~MyNtuple()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t MyNtuple::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t MyNtuple::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void MyNtuple::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_flav = 0;
   jet_truthflav = 0;
   trackjet_pt = 0;
   trackjet_eta = 0;
   trackjet_phi = 0;
   trackjet_e = 0;
   trackjet_isbjet = 0;
   trackjet_passISROR = 0;
   trackjet_ntrk = 0;
   trackjet_MV2c10 = 0;
   trackjet_flav = 0;
   trackjet_passDR = 0;
   trackjet_truthflav = 0;
   trackjet_truthflavhadexcl = 0;
   trackjet_origin = 0;
   trackjet_type = 0;
   el_pt = 0;
   el_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   tau_pt = 0;
   tau_eta = 0;
   tau_phi = 0;
   tau_e = 0;
   tau_bdtJet = 0;
   tau_bdtEl = 0;
   tau_nH = 0;
   tau_nTracks = 0;
   tau_nPi0 = 0;
   tau_nCharged = 0;
   tau_nNeut = 0;
   tau_passOR = 0;
   jet_MV2c10 = 0;
   jet_passTightClean = 0;
   jet_passTightCleanDFFlag = 0;
   jet_isSimpleTau = 0;
   jet_ntracks = 0;
   jet_truthflavhadexcl = 0;
   rcjet_kt12_pt = 0;
   rcjet_kt12_eta = 0;
   rcjet_kt12_phi = 0;
   rcjet_kt12_e = 0;
   rcjet_kt12_flav = 0;
   rcjet_kt15_pt = 0;
   rcjet_kt15_eta = 0;
   rcjet_kt15_phi = 0;
   rcjet_kt15_e = 0;
   rcjet_kt15_flav = 0;
   rcjet_kt10_pt = 0;
   rcjet_kt10_eta = 0;
   rcjet_kt10_phi = 0;
   rcjet_kt10_e = 0;
   rcjet_kt10_flav = 0;
   rcjet_kt8_pt = 0;
   rcjet_kt8_eta = 0;
   rcjet_kt8_phi = 0;
   rcjet_kt8_e = 0;
   rcjet_kt8_flav = 0;
   fatjet_kt10_pt = 0;
   fatjet_kt10_eta = 0;
   fatjet_kt10_phi = 0;
   fatjet_kt10_e = 0;
   fatjet_kt10_ntrkjets = 0;
   fatjet_kt10_nghostbhad = 0;
   fatjet_kt10_Split12 = 0;
   fatjet_kt10_Split23 = 0;
   fatjet_kt10_Split34 = 0;
   fatjet_kt10_Qw = 0;
   fatjet_kt10_Tau1 = 0;
   fatjet_kt10_Tau2 = 0;
   fatjet_kt10_Tau3 = 0;
   fatjet_kt10_Tau32 = 0;
   fatjet_kt10_ECF1 = 0;
   fatjet_kt10_ECF2 = 0;
   fatjet_kt10_ECF3 = 0;
   fatjet_kt10_DNNTop80 = 0;
   fatjet_kt10_D2 = 0;
   fatjet_kt10_C2 = 0;
   fatjet_kt10_origin = 0;
   sbv_Lxy = 0;
   sbv_X = 0;
   sbv_Y = 0;
   sbv_Z = 0;
   sbv_nTracks = 0;
   sbv_Chi2Reduced = 0;
   sbv_m = 0;
   sbv_pt = 0;
   sbv_eta = 0;
   sbv_phi = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("LumiWeight", &LumiWeight, &b_LumiWeight);
   fChain->SetBranchAddress("nBJets", &nBJets, &b_nBJets);
   fChain->SetBranchAddress("m_1rcjet_kt12", &m_1rcjet_kt12, &b_m_1rcjet_kt12);
   fChain->SetBranchAddress("m_2rcjet_kt12", &m_2rcjet_kt12, &b_m_2rcjet_kt12);
   fChain->SetBranchAddress("m_1rcjet_kt8", &m_1rcjet_kt8, &b_m_1rcjet_kt8);
   fChain->SetBranchAddress("m_2rcjet_kt8", &m_2rcjet_kt8, &b_m_2rcjet_kt8);
   fChain->SetBranchAddress("dsid", &dsid, &b_dsid);
   fChain->SetBranchAddress("AnalysisWeight", &AnalysisWeight, &b_AnalysisWeight);
   fChain->SetBranchAddress("pileupweight", &pileupweight, &b_pileupweight);
   fChain->SetBranchAddress("pileupweightUP", &pileupweightUP, &b_pileupweightUP);
   fChain->SetBranchAddress("pileupweightDOWN", &pileupweightDOWN, &b_pileupweightDOWN);
   fChain->SetBranchAddress("pileupweightHash", &pileupweightHash, &b_pileupweightHash);
   fChain->SetBranchAddress("nJets", &nJets, &b_nJets);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_flav", &jet_flav, &b_jet_flav);
   fChain->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
   fChain->SetBranchAddress("trackjet_pt", &trackjet_pt, &b_trackjet_pt);
   fChain->SetBranchAddress("trackjet_eta", &trackjet_eta, &b_trackjet_eta);
   fChain->SetBranchAddress("trackjet_phi", &trackjet_phi, &b_trackjet_phi);
   fChain->SetBranchAddress("trackjet_e", &trackjet_e, &b_trackjet_e);
   fChain->SetBranchAddress("trackjet_isbjet", &trackjet_isbjet, &b_trackjet_isbjet);
   fChain->SetBranchAddress("trackjet_passISROR", &trackjet_passISROR, &b_trackjet_passISROR);
   fChain->SetBranchAddress("trackjet_ntrk", &trackjet_ntrk, &b_trackjet_ntrk);
   fChain->SetBranchAddress("trackjet_MV2c10", &trackjet_MV2c10, &b_trackjet_MV2c10);
   fChain->SetBranchAddress("trackjet_flav", &trackjet_flav, &b_trackjet_flav);
   fChain->SetBranchAddress("trackjet_passDR", &trackjet_passDR, &b_trackjet_passDR);
   fChain->SetBranchAddress("trackjet_truthflav", &trackjet_truthflav, &b_trackjet_truthflav);
   fChain->SetBranchAddress("trackjet_truthflavhadexcl", &trackjet_truthflavhadexcl, &b_trackjet_truthflavhadexcl);
   fChain->SetBranchAddress("trackjet_origin", &trackjet_origin, &b_trackjet_origin);
   fChain->SetBranchAddress("trackjet_type", &trackjet_type, &b_trackjet_type);
   fChain->SetBranchAddress("nbaselineEl", &nbaselineEl, &b_nbaselineEl);
   fChain->SetBranchAddress("nEl", &nEl, &b_nEl);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("nbaselineMu", &nbaselineMu, &b_nbaselineMu);
   fChain->SetBranchAddress("nMu", &nMu, &b_nMu);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("nTau", &nTau, &b_nTau);
   fChain->SetBranchAddress("tau_pt", &tau_pt, &b_tau_pt);
   fChain->SetBranchAddress("tau_eta", &tau_eta, &b_tau_eta);
   fChain->SetBranchAddress("tau_phi", &tau_phi, &b_tau_phi);
   fChain->SetBranchAddress("tau_e", &tau_e, &b_tau_e);
   fChain->SetBranchAddress("tau_bdtJet", &tau_bdtJet, &b_tau_bdtJet);
   fChain->SetBranchAddress("tau_bdtEl", &tau_bdtEl, &b_tau_bdtEl);
   fChain->SetBranchAddress("tau_nH", &tau_nH, &b_tau_nH);
   fChain->SetBranchAddress("tau_nTracks", &tau_nTracks, &b_tau_nTracks);
   fChain->SetBranchAddress("tau_nPi0", &tau_nPi0, &b_tau_nPi0);
   fChain->SetBranchAddress("tau_nCharged", &tau_nCharged, &b_tau_nCharged);
   fChain->SetBranchAddress("tau_nNeut", &tau_nNeut, &b_tau_nNeut);
   fChain->SetBranchAddress("tau_passOR", &tau_passOR, &b_tau_passOR);
   fChain->SetBranchAddress("MET_pt", &MET_pt, &b_MET_pt);
   fChain->SetBranchAddress("MET_phi", &MET_phi, &b_MET_phi);
   fChain->SetBranchAddress("MET_pt_prime", &MET_pt_prime, &b_MET_pt_prime);
   fChain->SetBranchAddress("MET_phi_prime", &MET_phi_prime, &b_MET_phi_prime);
   fChain->SetBranchAddress("sumet", &sumet, &b_sumet);
   fChain->SetBranchAddress("metsig", &metsig, &b_metsig);
   fChain->SetBranchAddress("metsig_binflate", &metsig_binflate, &b_metsig_binflate);
   fChain->SetBranchAddress("metsigST", &metsigST, &b_metsigST);
   fChain->SetBranchAddress("metsigET", &metsigET, &b_metsigET);
   fChain->SetBranchAddress("metsigHT", &metsigHT, &b_metsigHT);
   fChain->SetBranchAddress("year", &year, &b_year);
   fChain->SetBranchAddress("MuonWeightReco", &MuonWeightReco, &b_MuonWeightReco);
   fChain->SetBranchAddress("MuonWeightTrigger", &MuonWeightTrigger, &b_MuonWeightTrigger);
   fChain->SetBranchAddress("ElecWeightReco", &ElecWeightReco, &b_ElecWeightReco);
   fChain->SetBranchAddress("ElecWeightTrigger", &ElecWeightTrigger, &b_ElecWeightTrigger);
   fChain->SetBranchAddress("TauWeight", &TauWeight, &b_TauWeight);
   fChain->SetBranchAddress("PrescaleWeight", &PrescaleWeight, &b_PrescaleWeight);
   fChain->SetBranchAddress("jvtweight", &jvtweight, &b_jvtweight);
   fChain->SetBranchAddress("btagweight", &btagweight, &b_btagweight);
   fChain->SetBranchAddress("btagtrkweight", &btagtrkweight, &b_btagtrkweight);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("averageIntPerXingCorr", &averageIntPerXingCorr, &b_averageIntPerXingCorr);
   fChain->SetBranchAddress("jet_MV2c10", &jet_MV2c10, &b_jet_MV2c10);
   fChain->SetBranchAddress("jet_passTightClean", &jet_passTightClean, &b_jet_passTightClean);
   fChain->SetBranchAddress("jet_passTightCleanDFFlag", &jet_passTightCleanDFFlag, &b_jet_passTightCleanDFFlag);
   fChain->SetBranchAddress("jet_isSimpleTau", &jet_isSimpleTau, &b_jet_isSimpleTau);
   fChain->SetBranchAddress("jet_ntracks", &jet_ntracks, &b_jet_ntracks);
   fChain->SetBranchAddress("jet_truthflavhadexcl", &jet_truthflavhadexcl, &b_jet_truthflavhadexcl);
   fChain->SetBranchAddress("passtauveto", &passtauveto, &b_passtauveto);
   fChain->SetBranchAddress("MET_jet_pt", &MET_jet_pt, &b_MET_jet_pt);
   fChain->SetBranchAddress("MET_jet_phi", &MET_jet_phi, &b_MET_jet_phi);
   fChain->SetBranchAddress("MET_mu_pt", &MET_mu_pt, &b_MET_mu_pt);
   fChain->SetBranchAddress("MET_mu_phi", &MET_mu_phi, &b_MET_mu_phi);
   fChain->SetBranchAddress("MET_el_pt", &MET_el_pt, &b_MET_el_pt);
   fChain->SetBranchAddress("MET_el_phi", &MET_el_phi, &b_MET_el_phi);
   fChain->SetBranchAddress("MET_y_pt", &MET_y_pt, &b_MET_y_pt);
   fChain->SetBranchAddress("MET_y_phi", &MET_y_phi, &b_MET_y_phi);
   fChain->SetBranchAddress("MET_softTrk_pt", &MET_softTrk_pt, &b_MET_softTrk_pt);
   fChain->SetBranchAddress("MET_softTrk_phi", &MET_softTrk_phi, &b_MET_softTrk_phi);
   fChain->SetBranchAddress("MET_track_pt", &MET_track_pt, &b_MET_track_pt);
   fChain->SetBranchAddress("MET_track_phi", &MET_track_phi, &b_MET_track_phi);
   fChain->SetBranchAddress("MET_NonInt_pt", &MET_NonInt_pt, &b_MET_NonInt_pt);
   fChain->SetBranchAddress("MET_NonInt_phi", &MET_NonInt_phi, &b_MET_NonInt_phi);
   fChain->SetBranchAddress("MET_tau_pt", &MET_tau_pt, &b_MET_tau_pt);
   fChain->SetBranchAddress("MET_tau_phi", &MET_tau_phi, &b_MET_tau_phi);
   fChain->SetBranchAddress("sumet_jet", &sumet_jet, &b_sumet_jet);
   fChain->SetBranchAddress("sumet_el", &sumet_el, &b_sumet_el);
   fChain->SetBranchAddress("sumet_y", &sumet_y, &b_sumet_y);
   fChain->SetBranchAddress("sumet_mu", &sumet_mu, &b_sumet_mu);
   fChain->SetBranchAddress("sumet_softTrk", &sumet_softTrk, &b_sumet_softTrk);
   fChain->SetBranchAddress("sumet_softClu", &sumet_softClu, &b_sumet_softClu);
   fChain->SetBranchAddress("sumet_NonInt", &sumet_NonInt, &b_sumet_NonInt);
   fChain->SetBranchAddress("METTrigPassed", &METTrigPassed, &b_METTrigPassed);
   fChain->SetBranchAddress("HLT_j400", &HLT_j400, &b_HLT_j400);
   fChain->SetBranchAddress("matched_HLT_j400", &matched_HLT_j400, &b_matched_HLT_j400);
   fChain->SetBranchAddress("HLT_j380", &HLT_j380, &b_HLT_j380);
   fChain->SetBranchAddress("matched_HLT_j380", &matched_HLT_j380, &b_matched_HLT_j380);
   fChain->SetBranchAddress("HLT_xe70_mht", &HLT_xe70_mht, &b_HLT_xe70_mht);
   fChain->SetBranchAddress("matched_HLT_xe70_mht", &matched_HLT_xe70_mht, &b_matched_HLT_xe70_mht);
   fChain->SetBranchAddress("HLT_xe100_L1XE50", &HLT_xe100_L1XE50, &b_HLT_xe100_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe100_L1XE50", &matched_HLT_xe100_L1XE50, &b_matched_HLT_xe100_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_tc_em_L1XE50", &HLT_xe100_tc_em_L1XE50, &b_HLT_xe100_tc_em_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe100_tc_em_L1XE50", &matched_HLT_xe100_tc_em_L1XE50, &b_matched_HLT_xe100_tc_em_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_mht_L1XE50", &HLT_xe110_mht_L1XE50, &b_HLT_xe110_mht_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe110_mht_L1XE50", &matched_HLT_xe110_mht_L1XE50, &b_matched_HLT_xe110_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_pueta_L1XE50", &HLT_xe110_pueta_L1XE50, &b_HLT_xe110_pueta_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe110_pueta_L1XE50", &matched_HLT_xe110_pueta_L1XE50, &b_matched_HLT_xe110_pueta_L1XE50);
   fChain->SetBranchAddress("HLT_xe120_pueta", &HLT_xe120_pueta, &b_HLT_xe120_pueta);
   fChain->SetBranchAddress("matched_HLT_xe120_pueta", &matched_HLT_xe120_pueta, &b_matched_HLT_xe120_pueta);
   fChain->SetBranchAddress("HLT_xe120_pufit", &HLT_xe120_pufit, &b_HLT_xe120_pufit);
   fChain->SetBranchAddress("matched_HLT_xe120_pufit", &matched_HLT_xe120_pufit, &b_matched_HLT_xe120_pufit);
   fChain->SetBranchAddress("HLT_xe120_tc_lcw_L1XE50", &HLT_xe120_tc_lcw_L1XE50, &b_HLT_xe120_tc_lcw_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe120_tc_lcw_L1XE50", &matched_HLT_xe120_tc_lcw_L1XE50, &b_matched_HLT_xe120_tc_lcw_L1XE50);
   fChain->SetBranchAddress("HLT_xe80_tc_lcw_L1XE50", &HLT_xe80_tc_lcw_L1XE50, &b_HLT_xe80_tc_lcw_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe80_tc_lcw_L1XE50", &matched_HLT_xe80_tc_lcw_L1XE50, &b_matched_HLT_xe80_tc_lcw_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_mht_L1XE50", &HLT_xe90_mht_L1XE50, &b_HLT_xe90_mht_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe90_mht_L1XE50", &matched_HLT_xe90_mht_L1XE50, &b_matched_HLT_xe90_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_mht_wEFMu_L1XE50", &HLT_xe90_mht_wEFMu_L1XE50, &b_HLT_xe90_mht_wEFMu_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe90_mht_wEFMu_L1XE50", &matched_HLT_xe90_mht_wEFMu_L1XE50, &b_matched_HLT_xe90_mht_wEFMu_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_tc_lcw_wEFMu_L1XE50", &HLT_xe90_tc_lcw_wEFMu_L1XE50, &b_HLT_xe90_tc_lcw_wEFMu_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe90_tc_lcw_wEFMu_L1XE50", &matched_HLT_xe90_tc_lcw_wEFMu_L1XE50, &b_matched_HLT_xe90_tc_lcw_wEFMu_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_pufit_L1XE50", &HLT_xe100_pufit_L1XE50, &b_HLT_xe100_pufit_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe100_pufit_L1XE50", &matched_HLT_xe100_pufit_L1XE50, &b_matched_HLT_xe100_pufit_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_pufit_L1XE55", &HLT_xe100_pufit_L1XE55, &b_HLT_xe100_pufit_L1XE55);
   fChain->SetBranchAddress("matched_HLT_xe100_pufit_L1XE55", &matched_HLT_xe100_pufit_L1XE55, &b_matched_HLT_xe100_pufit_L1XE55);
   fChain->SetBranchAddress("HLT_xe110_pufit_L1XE50", &HLT_xe110_pufit_L1XE50, &b_HLT_xe110_pufit_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe110_pufit_L1XE50", &matched_HLT_xe110_pufit_L1XE50, &b_matched_HLT_xe110_pufit_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_pufit_L1XE55", &HLT_xe110_pufit_L1XE55, &b_HLT_xe110_pufit_L1XE55);
   fChain->SetBranchAddress("matched_HLT_xe110_pufit_L1XE55", &matched_HLT_xe110_pufit_L1XE55, &b_matched_HLT_xe110_pufit_L1XE55);
   fChain->SetBranchAddress("HLT_xe110_pufit_L1XE60", &HLT_xe110_pufit_L1XE60, &b_HLT_xe110_pufit_L1XE60);
   fChain->SetBranchAddress("matched_HLT_xe110_pufit_L1XE60", &matched_HLT_xe110_pufit_L1XE60, &b_matched_HLT_xe110_pufit_L1XE60);
   fChain->SetBranchAddress("HLT_xe120_pufit_L1XE50", &HLT_xe120_pufit_L1XE50, &b_HLT_xe120_pufit_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe120_pufit_L1XE50", &matched_HLT_xe120_pufit_L1XE50, &b_matched_HLT_xe120_pufit_L1XE50);
   fChain->SetBranchAddress("HLT_xe120_pufit_L1XE55", &HLT_xe120_pufit_L1XE55, &b_HLT_xe120_pufit_L1XE55);
   fChain->SetBranchAddress("matched_HLT_xe120_pufit_L1XE55", &matched_HLT_xe120_pufit_L1XE55, &b_matched_HLT_xe120_pufit_L1XE55);
   fChain->SetBranchAddress("HLT_xe120_pufit_L1XE60", &HLT_xe120_pufit_L1XE60, &b_HLT_xe120_pufit_L1XE60);
   fChain->SetBranchAddress("matched_HLT_xe120_pufit_L1XE60", &matched_HLT_xe120_pufit_L1XE60, &b_matched_HLT_xe120_pufit_L1XE60);
   fChain->SetBranchAddress("HLT_xe90_pufit_L1XE50", &HLT_xe90_pufit_L1XE50, &b_HLT_xe90_pufit_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe90_pufit_L1XE50", &matched_HLT_xe90_pufit_L1XE50, &b_matched_HLT_xe90_pufit_L1XE50);
   fChain->SetBranchAddress("HLT_e24_lhmedium_nod0_L1EM20VH", &HLT_e24_lhmedium_nod0_L1EM20VH, &b_HLT_e24_lhmedium_nod0_L1EM20VH);
   fChain->SetBranchAddress("matched_HLT_e24_lhmedium_nod0_L1EM20VH", &matched_HLT_e24_lhmedium_nod0_L1EM20VH, &b_matched_HLT_e24_lhmedium_nod0_L1EM20VH);
   fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
   fChain->SetBranchAddress("matched_HLT_e120_lhloose", &matched_HLT_e120_lhloose, &b_matched_HLT_e120_lhloose);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("matched_HLT_e140_lhloose_nod0", &matched_HLT_e140_lhloose_nod0, &b_matched_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0_L1EM24VHI", &HLT_e140_lhloose_nod0_L1EM24VHI, &b_HLT_e140_lhloose_nod0_L1EM24VHI);
   fChain->SetBranchAddress("matched_HLT_e140_lhloose_nod0_L1EM24VHI", &matched_HLT_e140_lhloose_nod0_L1EM24VHI, &b_matched_HLT_e140_lhloose_nod0_L1EM24VHI);
   fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("matched_HLT_e24_lhmedium_L1EM20VH", &matched_HLT_e24_lhmedium_L1EM20VH, &b_matched_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("HLT_e24_lhtight_nod0_ivarloose", &HLT_e24_lhtight_nod0_ivarloose, &b_HLT_e24_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("matched_HLT_e24_lhtight_nod0_ivarloose", &matched_HLT_e24_lhtight_nod0_ivarloose, &b_matched_HLT_e24_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("matched_HLT_e26_lhtight_nod0_ivarloose", &matched_HLT_e26_lhtight_nod0_ivarloose, &b_matched_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM", &HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM, &b_HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM);
   fChain->SetBranchAddress("matched_HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM", &matched_HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM, &b_matched_HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM);
   fChain->SetBranchAddress("HLT_e28_lhtight_nod0_ivarloose", &HLT_e28_lhtight_nod0_ivarloose, &b_HLT_e28_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("matched_HLT_e28_lhtight_nod0_ivarloose", &matched_HLT_e28_lhtight_nod0_ivarloose, &b_matched_HLT_e28_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM", &HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM, &b_HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM);
   fChain->SetBranchAddress("matched_HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM", &matched_HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM, &b_matched_HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM);
   fChain->SetBranchAddress("HLT_e300_etcut", &HLT_e300_etcut, &b_HLT_e300_etcut);
   fChain->SetBranchAddress("matched_HLT_e300_etcut", &matched_HLT_e300_etcut, &b_matched_HLT_e300_etcut);
   fChain->SetBranchAddress("HLT_e300_etcut_L1EM24VHI", &HLT_e300_etcut_L1EM24VHI, &b_HLT_e300_etcut_L1EM24VHI);
   fChain->SetBranchAddress("matched_HLT_e300_etcut_L1EM24VHI", &matched_HLT_e300_etcut_L1EM24VHI, &b_matched_HLT_e300_etcut_L1EM24VHI);
   fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
   fChain->SetBranchAddress("matched_HLT_e60_lhmedium", &matched_HLT_e60_lhmedium, &b_matched_HLT_e60_lhmedium);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("matched_HLT_e60_lhmedium_nod0", &matched_HLT_e60_lhmedium_nod0, &b_matched_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0_L1EM24VHI", &HLT_e60_lhmedium_nod0_L1EM24VHI, &b_HLT_e60_lhmedium_nod0_L1EM24VHI);
   fChain->SetBranchAddress("matched_HLT_e60_lhmedium_nod0_L1EM24VHI", &matched_HLT_e60_lhmedium_nod0_L1EM24VHI, &b_matched_HLT_e60_lhmedium_nod0_L1EM24VHI);
   fChain->SetBranchAddress("HLT_e60_medium", &HLT_e60_medium, &b_HLT_e60_medium);
   fChain->SetBranchAddress("matched_HLT_e60_medium", &matched_HLT_e60_medium, &b_matched_HLT_e60_medium);
   fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("matched_HLT_mu20_iloose_L1MU15", &matched_HLT_mu20_iloose_L1MU15, &b_matched_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("HLT_mu24_iloose", &HLT_mu24_iloose, &b_HLT_mu24_iloose);
   fChain->SetBranchAddress("matched_HLT_mu24_iloose", &matched_HLT_mu24_iloose, &b_matched_HLT_mu24_iloose);
   fChain->SetBranchAddress("HLT_mu24_iloose_L1MU15", &HLT_mu24_iloose_L1MU15, &b_HLT_mu24_iloose_L1MU15);
   fChain->SetBranchAddress("matched_HLT_mu24_iloose_L1MU15", &matched_HLT_mu24_iloose_L1MU15, &b_matched_HLT_mu24_iloose_L1MU15);
   fChain->SetBranchAddress("HLT_mu24_imedium", &HLT_mu24_imedium, &b_HLT_mu24_imedium);
   fChain->SetBranchAddress("matched_HLT_mu24_imedium", &matched_HLT_mu24_imedium, &b_matched_HLT_mu24_imedium);
   fChain->SetBranchAddress("HLT_mu24_ivarloose", &HLT_mu24_ivarloose, &b_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("matched_HLT_mu24_ivarloose", &matched_HLT_mu24_ivarloose, &b_matched_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("HLT_mu24_ivarloose_L1MU15", &HLT_mu24_ivarloose_L1MU15, &b_HLT_mu24_ivarloose_L1MU15);
   fChain->SetBranchAddress("matched_HLT_mu24_ivarloose_L1MU15", &matched_HLT_mu24_ivarloose_L1MU15, &b_matched_HLT_mu24_ivarloose_L1MU15);
   fChain->SetBranchAddress("HLT_mu24_ivarmedium", &HLT_mu24_ivarmedium, &b_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("matched_HLT_mu24_ivarmedium", &matched_HLT_mu24_ivarmedium, &b_matched_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("HLT_mu26_imedium", &HLT_mu26_imedium, &b_HLT_mu26_imedium);
   fChain->SetBranchAddress("matched_HLT_mu26_imedium", &matched_HLT_mu26_imedium, &b_matched_HLT_mu26_imedium);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("matched_HLT_mu26_ivarmedium", &matched_HLT_mu26_ivarmedium, &b_matched_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_mu40", &HLT_mu40, &b_HLT_mu40);
   fChain->SetBranchAddress("matched_HLT_mu40", &matched_HLT_mu40, &b_matched_HLT_mu40);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("matched_HLT_mu50", &matched_HLT_mu50, &b_matched_HLT_mu50);
   fChain->SetBranchAddress("HLT_mu60", &HLT_mu60, &b_HLT_mu60);
   fChain->SetBranchAddress("matched_HLT_mu60", &matched_HLT_mu60, &b_matched_HLT_mu60);
   fChain->SetBranchAddress("HLT_mu60_0eta105_msonly", &HLT_mu60_0eta105_msonly, &b_HLT_mu60_0eta105_msonly);
   fChain->SetBranchAddress("matched_HLT_mu60_0eta105_msonly", &matched_HLT_mu60_0eta105_msonly, &b_matched_HLT_mu60_0eta105_msonly);
   fChain->SetBranchAddress("HLT_xe100_mht_L1XE50", &HLT_xe100_mht_L1XE50, &b_HLT_xe100_mht_L1XE50);
   fChain->SetBranchAddress("matched_HLT_xe100_mht_L1XE50", &matched_HLT_xe100_mht_L1XE50, &b_matched_HLT_xe100_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe70", &HLT_xe70, &b_HLT_xe70);
   fChain->SetBranchAddress("matched_HLT_xe70", &matched_HLT_xe70, &b_matched_HLT_xe70);
   fChain->SetBranchAddress("HLT_xe70_tc_lcw", &HLT_xe70_tc_lcw, &b_HLT_xe70_tc_lcw);
   fChain->SetBranchAddress("matched_HLT_xe70_tc_lcw", &matched_HLT_xe70_tc_lcw, &b_matched_HLT_xe70_tc_lcw);
   fChain->SetBranchAddress("GenFiltMET", &GenFiltMET, &b_GenFiltMET);
   fChain->SetBranchAddress("GenFiltHT", &GenFiltHT, &b_GenFiltHT);
   fChain->SetBranchAddress("rcjet_kt12_pt", &rcjet_kt12_pt, &b_rcjet_kt12_pt);
   fChain->SetBranchAddress("rcjet_kt12_eta", &rcjet_kt12_eta, &b_rcjet_kt12_eta);
   fChain->SetBranchAddress("rcjet_kt12_phi", &rcjet_kt12_phi, &b_rcjet_kt12_phi);
   fChain->SetBranchAddress("rcjet_kt12_e", &rcjet_kt12_e, &b_rcjet_kt12_e);
   fChain->SetBranchAddress("rcjet_kt12_flav", &rcjet_kt12_flav, &b_rcjet_kt12_flav);
   fChain->SetBranchAddress("rcjet_kt15_pt", &rcjet_kt15_pt, &b_rcjet_kt15_pt);
   fChain->SetBranchAddress("rcjet_kt15_eta", &rcjet_kt15_eta, &b_rcjet_kt15_eta);
   fChain->SetBranchAddress("rcjet_kt15_phi", &rcjet_kt15_phi, &b_rcjet_kt15_phi);
   fChain->SetBranchAddress("rcjet_kt15_e", &rcjet_kt15_e, &b_rcjet_kt15_e);
   fChain->SetBranchAddress("rcjet_kt15_flav", &rcjet_kt15_flav, &b_rcjet_kt15_flav);
   fChain->SetBranchAddress("rcjet_kt10_pt", &rcjet_kt10_pt, &b_rcjet_kt10_pt);
   fChain->SetBranchAddress("rcjet_kt10_eta", &rcjet_kt10_eta, &b_rcjet_kt10_eta);
   fChain->SetBranchAddress("rcjet_kt10_phi", &rcjet_kt10_phi, &b_rcjet_kt10_phi);
   fChain->SetBranchAddress("rcjet_kt10_e", &rcjet_kt10_e, &b_rcjet_kt10_e);
   fChain->SetBranchAddress("rcjet_kt10_flav", &rcjet_kt10_flav, &b_rcjet_kt10_flav);
   fChain->SetBranchAddress("rcjet_kt8_pt", &rcjet_kt8_pt, &b_rcjet_kt8_pt);
   fChain->SetBranchAddress("rcjet_kt8_eta", &rcjet_kt8_eta, &b_rcjet_kt8_eta);
   fChain->SetBranchAddress("rcjet_kt8_phi", &rcjet_kt8_phi, &b_rcjet_kt8_phi);
   fChain->SetBranchAddress("rcjet_kt8_e", &rcjet_kt8_e, &b_rcjet_kt8_e);
   fChain->SetBranchAddress("rcjet_kt8_flav", &rcjet_kt8_flav, &b_rcjet_kt8_flav);
   fChain->SetBranchAddress("fatjet_kt10_pt", &fatjet_kt10_pt, &b_fatjet_kt10_pt);
   fChain->SetBranchAddress("fatjet_kt10_eta", &fatjet_kt10_eta, &b_fatjet_kt10_eta);
   fChain->SetBranchAddress("fatjet_kt10_phi", &fatjet_kt10_phi, &b_fatjet_kt10_phi);
   fChain->SetBranchAddress("fatjet_kt10_e", &fatjet_kt10_e, &b_fatjet_kt10_e);
   fChain->SetBranchAddress("fatjet_kt10_ntrkjets", &fatjet_kt10_ntrkjets, &b_fatjet_kt10_ntrkjets);
   fChain->SetBranchAddress("fatjet_kt10_nghostbhad", &fatjet_kt10_nghostbhad, &b_fatjet_kt10_nghostbhad);
   fChain->SetBranchAddress("fatjet_kt10_Split12", &fatjet_kt10_Split12, &b_fatjet_kt10_Split12);
   fChain->SetBranchAddress("fatjet_kt10_Split23", &fatjet_kt10_Split23, &b_fatjet_kt10_Split23);
   fChain->SetBranchAddress("fatjet_kt10_Split34", &fatjet_kt10_Split34, &b_fatjet_kt10_Split34);
   fChain->SetBranchAddress("fatjet_kt10_Qw", &fatjet_kt10_Qw, &b_fatjet_kt10_Qw);
   fChain->SetBranchAddress("fatjet_kt10_Tau1", &fatjet_kt10_Tau1, &b_fatjet_kt10_Tau1);
   fChain->SetBranchAddress("fatjet_kt10_Tau2", &fatjet_kt10_Tau2, &b_fatjet_kt10_Tau2);
   fChain->SetBranchAddress("fatjet_kt10_Tau3", &fatjet_kt10_Tau3, &b_fatjet_kt10_Tau3);
   fChain->SetBranchAddress("fatjet_kt10_Tau32", &fatjet_kt10_Tau32, &b_fatjet_kt10_Tau32);
   fChain->SetBranchAddress("fatjet_kt10_ECF1", &fatjet_kt10_ECF1, &b_fatjet_kt10_ECF1);
   fChain->SetBranchAddress("fatjet_kt10_ECF2", &fatjet_kt10_ECF2, &b_fatjet_kt10_ECF2);
   fChain->SetBranchAddress("fatjet_kt10_ECF3", &fatjet_kt10_ECF3, &b_fatjet_kt10_ECF3);
   fChain->SetBranchAddress("fatjet_kt10_DNNTop80", &fatjet_kt10_DNNTop80, &b_fatjet_kt10_DNNTop80);
   fChain->SetBranchAddress("fatjet_kt10_D2", &fatjet_kt10_D2, &b_fatjet_kt10_D2);
   fChain->SetBranchAddress("fatjet_kt10_C2", &fatjet_kt10_C2, &b_fatjet_kt10_C2);
   fChain->SetBranchAddress("fatjet_kt10_origin", &fatjet_kt10_origin, &b_fatjet_kt10_origin);
   fChain->SetBranchAddress("sbv_Lxy", &sbv_Lxy, &b_sbv_Lxy);
   fChain->SetBranchAddress("sbv_X", &sbv_X, &b_sbv_X);
   fChain->SetBranchAddress("sbv_Y", &sbv_Y, &b_sbv_Y);
   fChain->SetBranchAddress("sbv_Z", &sbv_Z, &b_sbv_Z);
   fChain->SetBranchAddress("sbv_nTracks", &sbv_nTracks, &b_sbv_nTracks);
   fChain->SetBranchAddress("sbv_Chi2Reduced", &sbv_Chi2Reduced, &b_sbv_Chi2Reduced);
   fChain->SetBranchAddress("sbv_m", &sbv_m, &b_sbv_m);
   fChain->SetBranchAddress("sbv_pt", &sbv_pt, &b_sbv_pt);
   fChain->SetBranchAddress("sbv_eta", &sbv_eta, &b_sbv_eta);
   fChain->SetBranchAddress("sbv_phi", &sbv_phi, &b_sbv_phi);
   fChain->SetBranchAddress("nsbv", &nsbv, &b_nsbv);
   Notify();
}

Bool_t MyNtuple::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void MyNtuple::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t MyNtuple::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef MyNtuple_cxx
